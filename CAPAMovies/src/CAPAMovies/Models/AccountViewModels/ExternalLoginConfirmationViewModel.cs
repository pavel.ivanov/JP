﻿using System.ComponentModel.DataAnnotations;

namespace CAPAMovies.Models.AccountViewModels
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
