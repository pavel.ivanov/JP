﻿using System.ComponentModel.DataAnnotations;

namespace CAPAMovies.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
