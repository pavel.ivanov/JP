﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Migrations.Operations;

namespace CAPAMovies.Models
{
    public class Format
    {
        [Key]
        [Display(Name = "*Format ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Range(2, 10, ErrorMessage = "Please Pick a Valid Format")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "*Format")]
        public string FormatName { get; set; }
        
        [Display(Name = "Physical Copy")]
        public bool Physical { get; set; } = true;
        
        private bool IsValid => !string.IsNullOrEmpty(FormatName);
    }
}
