﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CAPAMovies.Models.MovieViewModels
{
    public class MovieDetails
    {
        public string Title;
        public int Year;
        public string Rating;
        public string Released;
        public string Runtime;
        public string Genre;
        public string Director;
        public string Writer;
        public string Actors;
        public string Plot;
        public string Language;
        public string Country;
        public string Awards;
        public string Poster;
        public string Metascore;
        public string ImdbRating;
        public string imdbID;
        public UserMovie myUserMovie;
        public Movie myMovie;

        public MovieDetails(UserMovie newUserMovie, Movie newMovie) {
            myUserMovie = newUserMovie;
            myMovie = newMovie;
        }
    }
}
