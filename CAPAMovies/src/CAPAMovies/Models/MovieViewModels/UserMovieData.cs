﻿using System.Collections.Generic;

namespace CAPAMovies.Models.MovieViewModels
{
    public class UserMovieData
    {
        public IEnumerable<ApplicationUser> Users { get; set; }
        public IEnumerable<Movie> Movies { get; set; }
    }
}
