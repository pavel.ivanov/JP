﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CAPAMovies.Models
{
    public class Movie
    {
        [Key]
        [Display(Name = "*IMDB ID")]
        public string ID { get; set; }

        [Required]
        [Display(Name = "*Title")]
        public string Title { get; set; }
        
        [Required]
        [Display(Name = "*Release Year")]
        [Range(1900, 2100)]
        public int Year { get; set; }

        [Url]
        [Display(Name = "Picture Link")]
        public string PicLink { get; set; } = "~/images/MovieUnavailable.png";
    }
}

/*
 * Association: Specifies that an entity member represents a data relationship, such as a foreign key relationship.
 * BindableType: Specifies whether a type is typically used for binding.
 * Compare: Provides an attribut that compares two properties
 * ConcurrencyCheck: Specifies that a property participates in optimistic concurrency checks.
 * CreditCard: Specifies that a data field value is a credit card number
 * CustomValidation: Specifies a custom validation method that is used to validate a property or class instance.
 * DataType: Specifies the name of an additional type to associate with a data field.
 * Display: Provides a general-purpose attribute that lets you specify localizable strings for types and members of entity partial classes.
 * DisplayColumn: Specifies the column that is displayed in the referred table as a foreign-key column
 * DisplayFormat: Specifies how data fields are displayed and formatted by ASP.NET Dynamic Data.
 * Editable: Indicates whether a data field is editable.
 * EmailAddress: Validates an email address.
 * EnumDataType: Enables a .NET Framework enumeration to be mapped to a data column.
 * FileExtensions: Validates file name extensions.
 * FilterUIHint: Represents an attribute that is used to specify the filtering behavior for a column.
 * Key: Denotes one or more properties that uniquely identify an entity.
 * MaxLength: Specifies the maximum length of an array or string data allowed in a property.
 * MetadataType: Specifies the metadata class to associate with a data model class.
 * MinLength: Specifies the minimum length of array or string data allowed in a property.
 * Phone: Specifies that a data field value is a well-formed phone number using a regular expression for phone numbers.
 * Range: Specifies the numeric range constraints for the value of a data field.
 * RegularExpression: Specifies that a data field value in ASP.NET Dynamic Data must match the specified regular expression.
 * Required: Specifies that a data field value is required.
 * ScaffoldColumn: Specifies whether a class or data column uses scaffolding.
 * ScaffoldTable: Specifies whether a class or data table uses scaffolding.
 * StringLength: Specifies the minimum and maximum length of characters that are allowed in a data field.
 * Timestamp: Specifies the data type of the column as a row version.
 * UIHint: Specifies the template or user control that Dynamic Data uses to display a data field.
 * Url: Provides URL validation.
 * Validation: Serves as the base class for all validation attributes.
 * 
 * OTHERS
 * DatabaseGenerated
*/
