﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CAPAMovies.Models
{
    public class UserMovie
    {
        public UserMovie() { }

        public UserMovie(string userId, string movieId, int formatId)
        {
            UserID = userId;
            MovieID = movieId;
            FormatID = formatId;
        }
        
        [Key, ForeignKey("User")]
        public string UserID { get; set; }

        [Key, ForeignKey("Movie")]
        public string MovieID { get; set; }

        [Range(2, 10, ErrorMessage = "Please Pick a Valid Format")]
        [Key, ForeignKey("Format")]
        public int FormatID { get; set; }

        [Display(Name = "Title")]
        public string TitleSort { get; set; }

        [DataType(DataType.Date)]
        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date Added")]
        [Range(typeof(DateTime), "1900-01-01", "2100-01-01",
            ErrorMessage = "Value for {0} must be between {1} and {2}")]
        public virtual DateTime DateAdded { get; set; }

        [Range(1, 5, ErrorMessage = "Give a rating between 1 and 5 stars")]
        [Display(Name = "Rating")]
        [DisplayFormat(NullDisplayText = "Not Rated")]
        public int? StarRating { get; set; }

        public string Location { get; set; }

        public string Custom { get; set; }

        [Display(Name = "Loaned To")]
        public string LoanedTo { get; set; }

        public ApplicationUser User { get; set; }
        public Movie Movie { get; set; }
        public Format Format { get; set; }

        public bool IsLoanable() => Format.Physical.Equals(true);
        public bool IsLoaned() => !string.IsNullOrEmpty(LoanedTo);
        public void CheckIn() => LoanedTo = null;
    }
}
