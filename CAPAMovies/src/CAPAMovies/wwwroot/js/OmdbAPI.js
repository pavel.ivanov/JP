﻿$PicContainer = $("#FlickPic");
$IdContainer = $("#omdbID");
$YearContainer = $("#omdbYear");
$PicLinkContainer = $("#omdbLink");
$TitleContainer = $("#titleLink");

$(document).ready(function(){
$("#SearchButton").click(function (p_oEvent) {
    var sUrl, sMovie, oData;
    p_oEvent.preventDefault();
    sMovie = this.form[0].value;
    sUrl = 'https://www.omdbapi.com/?s=' + sMovie + '&type=movie' + '&apikey=ed229b04';
    $.ajax(sUrl, {
        complete: function (p_oXHR, p_sStatus) {
            oData = $.parseJSON(p_oXHR.responseText);
            console.log(oData);
            var moviePix = "";
            for (var movieIdx in oData.Search) {
                if (movieIdx >= 6) {
                    continue;
                }
                var newMovie = oData.Search[movieIdx];
                moviePix += '<div class="moviePicker" id=' + newMovie.imdbID + '><img class="posterimage" src="' + newMovie.Poster + '"/></div>';
            }
            $PicContainer.find('.poster').html(moviePix);
        }
    });
});

$(".poster").on("click", ".moviePicker", function () {
    event.stopPropagation();
    var myMovie = event.path[1];
    setInfo(myMovie.id);
});
});

function setInfo(movieId) {
    var sUrl, oData;
    sUrl = 'https://www.omdbapi.com/?i=' + movieId + '&apikey=ed229b04';
    $.ajax(sUrl, {
        complete: function (p_oXHR, p_sStatus) {
            oData = $.parseJSON(p_oXHR.responseText);
            console.log(oData);

            $TitleContainer[0].value = oData.Title;
            $IdContainer[0].value = oData.imdbID;
            $YearContainer[0].value = oData.Year;
            $PicLinkContainer[0].value = oData.Poster;
            $TitleContainer[0].innerHTML = oData.Title;
            $IdContainer[0].innerHTML = oData.imdbID;
            $YearContainer[0].innerHTML = oData.Year;
            $PicLinkContainer[0].innerHTML = oData.Poster;
        }
    });
}