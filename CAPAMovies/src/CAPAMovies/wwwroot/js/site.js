﻿﻿$(document).ready(function () {
    if (this.URL.indexOf('gridview') >= 0) {
        $("#ListViewContainer").hide();
        $("#GridViewContainer").show();
    }
    $('#list').click(function (event) {
        $("#ListViewContainer").show();
        $("#GridViewContainer").hide();
        $(this).attr('href', function () {
            if (!(this.href.indexOf('listview') === (this.href.length - 8)))
                return this.href + 'listview';
        });
    });
    $('#grid').click(function (event) {
        $("#ListViewContainer").hide();
        $("#GridViewContainer").show();
        $(this).attr('href', function () {
            if (!(this.href.indexOf('gridview') === (this.href.length - 8)))
                return this.href + 'gridview';
        });
    });
});
