﻿using System.Threading.Tasks;

namespace CAPAMovies.Services
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}
