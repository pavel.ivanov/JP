﻿using System.Threading.Tasks;

namespace CAPAMovies.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
