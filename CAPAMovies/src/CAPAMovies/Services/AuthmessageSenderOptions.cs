﻿// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace CAPAMovies.Services
{
        public class AuthMessageSenderOptions
        {
            public string SendGridUser { get; set; }
            public string SendGridKey { get; set; }
        }
}
