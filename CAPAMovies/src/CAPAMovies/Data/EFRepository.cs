﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using CAPAMovies.Models;
using Microsoft.AspNetCore.Mvc.TagHelpers.Internal;
using Microsoft.EntityFrameworkCore;

namespace CAPAMovies.Data
{
    public class EFRepository : IRepository
    {
        private readonly ApplicationDbContext _context;

        public EFRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<ApplicationUser> Users
        {
            get { return _context.Users; }
            set
            {
                foreach (var x in _context.Users)
                {
                    Remove(x);
                }
                foreach (var x in value)
                {
                    _context.Add(x);
                }
            }
        }

        public IEnumerable<Format> Formats
        {
            get { return _context.Formats; }
            set
            {
                foreach (var x in _context.Formats)
                {
                    Remove(x);
                }
                foreach (var x in value)
                {
                    _context.Add(x);
                }
            }
        }

        public IEnumerable<Movie> Movies
        {
            get { return _context.Movies; }
            set
            {
                foreach (var x in _context.Movies)
                {
                    Remove(x);
                }
                foreach (var x in value)
                {
                    _context.Add(x);
                }
            }
        }

        public IEnumerable<UserMovie> UserMovies
        {
            get { return _context.UserMovies; }
            set
            {
                foreach (var x in _context.UserMovies)
                {
                    Remove(x);
                }
                foreach (var x in value)
                {
                    _context.Add(x);
                }
            }
        }

        private IEnumerable<ApplicationUser> AddUsers = new List<ApplicationUser>();
        private IEnumerable<Format> AddFormats = new List<Format>();
        private IEnumerable<Movie> AddMovies = new List<Movie>();
        private IEnumerable<UserMovie> AddUserMovies = new List<UserMovie>();

        private IEnumerable<ApplicationUser> UpdateUsers = new List<ApplicationUser>();
        private IEnumerable<Format> UpdateFormats = new List<Format>();
        private IEnumerable<Movie> UpdateMovies = new List<Movie>();
        private IEnumerable<UserMovie> UpdateUserMovies = new List<UserMovie>();

        private IEnumerable<ApplicationUser> RemoveUsers = new List<ApplicationUser>();
        private IEnumerable<Format> RemoveFormats = new List<Format>();
        private IEnumerable<Movie> RemoveMovies = new List<Movie>();
        private IEnumerable<UserMovie> RemoveUserMovies = new List<UserMovie>();

        //ADD

        public void Add(ApplicationUser user)
        {
            _context.Add(user);
            AddUsers.Append(user);
        }

        public void Add(Format format)
        {
            _context.Add(format);
            AddFormats.Append(format);
        }

        public void Add(Movie movie)
        {
            _context.Add(movie);
            AddMovies.Append(movie);
        }

        public void Add(UserMovie usermovie)
        {
            _context.Add(usermovie);
            AddUserMovies.Append(usermovie);
        }

        //UPDATE

        public void Update(ApplicationUser user)
        {
            _context.Update(user);
            UpdateUsers.Append(user);
        }

        public void Update(Format format)
        {
            _context.Update(format);
            UpdateFormats.Append(format);
        }

        public void Update(Movie movie)
        {
            _context.Update(movie);
            UpdateMovies.Append(movie);
        }

        public void Update(UserMovie usermovie)
        {
            _context.Update(usermovie);
            UpdateUserMovies.Append(usermovie);
        }

        //REMOVE

        public void Remove(ApplicationUser user)
        {
            foreach (var m in _context.UserMovies)
            {
                if (m.UserID == user.Id)
                {
                    _context.Remove(m);
                    RemoveUserMovies.Append(m);
                }
            }
            _context.Remove(user);
            RemoveUsers.Append(user);
        }

        public void Remove(Format format)
        {
            _context.Remove(format);
            RemoveFormats.Append(format);
        }

        public void Remove(Movie movie)
        {
            _context.Remove(movie);
            RemoveMovies.Append(movie);
        }

        public void Remove(UserMovie usermovie)
        {
            _context.Remove(usermovie);
            RemoveUserMovies.Append(usermovie);
        }
        //*****************************************

        public async Task SaveChangesAsync()
        {
            //ADD
            await _context.SaveChangesAsync();
            foreach (var x in AddUsers)
            {
                Users.Append(x);
            }
            AddUsers = new List<ApplicationUser>();
            foreach (var x in AddFormats)
            {
                Formats.Append(x);
            }
            AddFormats = new List<Format>();
            foreach (var x in AddMovies)
            {
                Movies.Append(x);
            }
            AddMovies = new List<Movie>();
            foreach (var x in AddUserMovies)
            {
                UserMovies.Append(x);
            }
            AddUserMovies = new List<UserMovie>();


            //UPDATE
            foreach (var x in UpdateUsers)
            {
                var users = Users.Select(y => y.Id == x.Id ? x : y).ToList();
                Users = users;
            }
            foreach (var x in UpdateFormats)
            {
                var formats = Formats.Select(y => y.ID == x.ID ? x : y).ToList();
                Formats = formats;
            }
            foreach (var x in UpdateMovies)
            {
                var movies = Movies.Select(y => y.ID == x.ID ? x : y).ToList();
                Movies = movies;
            }
            foreach (var x in UpdateUserMovies)
            {
                var usermovies = new List<UserMovie>();
                foreach (var y in UserMovies)
                {
                    if (y.UserID == x.UserID && y.MovieID == x.MovieID && y.FormatID == x.FormatID)
                    {
                        Add(x);
                    }
                    else
                    {
                        Add(y);
                    }
                }
                UserMovies = usermovies;
            }


            //REMOVE
            foreach (var x in RemoveUsers)
            {
                var users = Users.Where(y => y.Id != x.Id).ToList();
                Users = users;
            }
            foreach (var x in RemoveFormats)
            {
                var formats = Formats.Where(y => y.ID != x.ID).ToList();
                Formats = formats;
            }
            foreach (var x in RemoveMovies)
            {
                var movies = Movies.Where(y => y.ID != x.ID).ToList();
                Movies = movies;
            }
            foreach (var x in RemoveUserMovies)
            {
                var usermovies = new List<UserMovie>();
                foreach (var y in UserMovies)
                {
                    if (y.UserID != x.UserID && y.MovieID != x.MovieID && y.FormatID != x.FormatID)
                    {
                        Add(y);
                    }
                }
                UserMovies = usermovies;
            }
        }
    }
}
