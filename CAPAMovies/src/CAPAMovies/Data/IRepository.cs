﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CAPAMovies.Models;

namespace CAPAMovies.Data
{
    public interface IRepository
    {
        IEnumerable<ApplicationUser> Users { get; set; }
        IEnumerable<Format> Formats { get; set; }
        IEnumerable<Movie> Movies { get; set; }
        IEnumerable<UserMovie> UserMovies { get; set; }

        void Add(ApplicationUser x);
        void Add(Format x);
        void Add(Movie x);
        void Add(UserMovie x);

        void Update(ApplicationUser x);
        void Update(Format x);
        void Update(Movie x);
        void Update(UserMovie x);

        void Remove(ApplicationUser x);
        void Remove(Format x);
        void Remove(Movie x);
        void Remove(UserMovie x);

        Task SaveChangesAsync();
    }
}
