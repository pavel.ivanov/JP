﻿using System;
using System.Linq;
using CAPAMovies.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.DependencyInjection;

namespace CAPAMovies.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<UserMovie> UserMovies { get; set; }
        public DbSet<Format> Formats { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
            
            builder.Entity<UserMovie>().HasKey(k => new { k.UserID, k.MovieID, k.FormatID }); //Set composite key
            builder.Entity<UserMovie>().HasOne(f => f.User).WithMany().OnDelete(DeleteBehavior.Restrict); //Do not delete the User when UserMovie is deleted
            builder.Entity<UserMovie>().HasOne(f => f.Movie).WithMany().OnDelete(DeleteBehavior.Restrict); //Do not delete the Movie when UserMovie is deleted
            builder.Entity<UserMovie>().HasOne(f => f.Format).WithMany().OnDelete(DeleteBehavior.Restrict); //Do not delete the Format when UserMovie is deleted

            builder.Entity<Format>().HasAlternateKey(f => f.FormatName); //Add unique constraint
        }

        public static void Seed(IApplicationBuilder app)
        {
            SeedUsers(app);
            SeedFormats(app);
            SeedMovies(app);
            SeedUserMovies(app);
        }

        public static void SeedUsers(IApplicationBuilder app)
        {
            var context = app.ApplicationServices.GetRequiredService<ApplicationDbContext>();
            context.Database.EnsureCreated();

            string[] userEmail = { "admin@user.com", "crystalstevens@capamovies.com", "alexhouston@capamovies.com", "paulivanov@capamovies.com", "aaronday@capamovies.com" };
            const string password = "Secret123$";
            var userManager = app.ApplicationServices.GetRequiredService<UserManager<ApplicationUser>>();
            var roleManager = app.ApplicationServices.GetRequiredService<RoleManager<IdentityRole>>();

            foreach (var s in userEmail)
            {
                var user = userManager.Users.SingleOrDefault(u => u.Email == s);
                if (user != null) continue;
                user = new ApplicationUser()
                {
                    UserName = s,
                    Email = s,
                    EmailConfirmed = true
                };
                var result = userManager.CreateAsync(user, password).Result;
                context.SaveChanges();
            }
            
            if (!roleManager.RoleExistsAsync("Admin").Result)
            {
                var result = roleManager.CreateAsync(new IdentityRole { Name = "Admin" }).Result;
                context.SaveChanges();
            }

            var manager = userManager.Users.SingleOrDefault(u => u.Email == "admin@user.com");
            if (manager != null)
            {
                var result = userManager.AddToRoleAsync(manager, "Admin").Result;
                context.SaveChanges();
            }
        }

        public static void SeedFormats(IApplicationBuilder app)
        {
            var context = app.ApplicationServices.GetRequiredService<ApplicationDbContext>();
            if (context.Formats.AsNoTracking().Any()) return;

            var format = new[]
            {
                new Format { FormatName =  "None", Physical = false },
                new Format { FormatName =  "DVD", Physical = true },
                new Format { FormatName =  "Blu-Ray", Physical = true },
                new Format { FormatName =  "3D Blu-Ray", Physical = true },
                new Format { FormatName =  "iTunes", Physical = false },
                new Format { FormatName =  "UltraViolet", Physical = false },
                new Format { FormatName =  "MP4", Physical = false },
                new Format { FormatName =  "AVI", Physical = false },
                new Format { FormatName =  "HD", Physical = true },
                new Format { FormatName =  "VHS", Physical = true }
            };

            context.Formats.AddRange(format);
            context.SaveChanges();
        }

        public static void SeedMovies(IApplicationBuilder app)
        {
            var context = app.ApplicationServices.GetRequiredService<ApplicationDbContext>();
            if (context.Movies.AsNoTracking().Any()) return;

            var movies = new[]
            {
                    new Movie
                    {
                        ID = "tt0076759",
                        PicLink = "https://images-na.ssl-images-amazon.com/images/M/MV5BYzQ2OTk4N2QtOGQwNy00MmI3LWEwNmEtOTk0OTY3NDk2MGJkL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_UX182_CR0,0,182,268_AL_.jpg",
                        Title = "Star Wars: Episode IV - A New Hope",
                        Year = 1977
                    },
                    new Movie
                    {
                        ID = "tt0458339",
                        PicLink = "https://images-na.ssl-images-amazon.com/images/M/MV5BMTYzOTc2NzU3N15BMl5BanBnXkFtZTcwNjY3MDE3NQ@@._V1_UX182_CR0,0,182,268_AL_.jpg",
                        Title = "Captain America: The First Avenger",
                        Year = 2011
                    },
                    new Movie
                    {
                        ID = "tt0120815",
                        PicLink = "https://images-na.ssl-images-amazon.com/images/M/MV5BZjhkMDM4MWItZTVjOC00ZDRhLThmYTAtM2I5NzBmNmNlMzI1XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_SX300.jpg",
                        Title = "Saving Private Ryan",
                        Year = 1998
                    },
                    new Movie
                    {
                        ID = "tt0172495",
                        PicLink = "https://images-na.ssl-images-amazon.com/images/M/MV5BMTgwMzQzNTQ1Ml5BMl5BanBnXkFtZTgwMDY2NTYxMTE@._V1_SX300.jpg",
                        Title = "Gladiator",
                        Year = 2000
                    },
                    new Movie
                    {
                        ID = "tt0988045",
                        PicLink = "https://images-na.ssl-images-amazon.com/images/M/MV5BMTg0NjEwNjUxM15BMl5BanBnXkFtZTcwMzk0MjQ5Mg@@._V1_SX300.jpg",
                        Title = "Sherlock Holmes",
                        Year = 2009
                    },
                    new Movie
                    {
                        ID = "tt0258463",
                        PicLink = "https://images-na.ssl-images-amazon.com/images/M/MV5BM2JkNGU0ZGMtZjVjNS00NjgyLWEyOWYtZmRmZGQyN2IxZjA2XkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_SX300.jpg",
                        Title = "The Bourne Identity",
                        Year = 2002
                    },
                    new Movie
                    {
                        ID = "tt0499549",
                        PicLink = "https://images-na.ssl-images-amazon.com/images/M/MV5BMTYwOTEwNjAzMl5BMl5BanBnXkFtZTcwODc5MTUwMw@@._V1_SX300.jpg",
                        Title = "Avatar",
                        Year = 2009
                    },
                    new Movie
                    {
                        ID = "tt0109830",
                        PicLink = "https://images-na.ssl-images-amazon.com/images/M/MV5BYThjM2MwZGMtMzg3Ny00NGRkLWE4M2EtYTBiNWMzOTY0YTI4XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_UY268_CR10,0,182,268_AL_.jpg",
                        Title = "Forest Gump",
                        Year = 1994
                    },
                    new Movie
                    {
                        ID = "tt0099685",
                        PicLink = "https://images-na.ssl-images-amazon.com/images/M/MV5BNThjMzczMjctZmIwOC00NTQ4LWJhZWItZDdhNTk5ZTdiMWFlXkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_SX300.jpg",
                        Title = "Goodfellas",
                        Year = 1990
                    },
                    new Movie
                    {
                        ID = "tt0169547",
                        PicLink = "https://images-na.ssl-images-amazon.com/images/M/MV5BMjM4NTI5NzYyNV5BMl5BanBnXkFtZTgwNTkxNTYxMTE@._V1_SX300.jpg",
                        Title = "American Beauty",
                        Year = 1999
                    }
                };

            context.Movies.AddRange(movies);
            context.SaveChanges();
        }

        public static void SeedUserMovies(IApplicationBuilder app)
        {
            var context = app.ApplicationServices.GetRequiredService<ApplicationDbContext>();
            if (context.UserMovies.AsNoTracking().Any()) return;
            var userManager = app.ApplicationServices.GetRequiredService<UserManager<ApplicationUser>>();
            var ids = userManager.Users.Where(u => u.Email.EndsWith("@capamovies.com")).Select(u => u.Id).ToList();
            if (ids.Count() < 4) return;
            var userMovies = new[]
            {
                    new UserMovie
                    {
                        UserID = ids[0],
                        MovieID = "tt0076759",
                        FormatID = 2,
                        TitleSort = "Star Wars: Episode IV - A New Hope",
                        DateAdded = new DateTime(2017, 1, 1),
                        StarRating = 3,
                        Location = "Shelf 1"
                    },
                    new UserMovie
                    {
                        UserID = ids[0],
                        MovieID = "tt0076759",
                        FormatID = 3,
                        TitleSort = "Star Wars: Episode IV - A New Hope",
                        DateAdded = new DateTime(2017, 1, 2),
                        StarRating = 3,
                        Location = "Shelf 1"
                    },
                    new UserMovie
                    {
                        UserID = ids[0],
                        MovieID = "tt0258463",
                        FormatID = 6,
                        TitleSort = "Bourne Identity, The",
                        DateAdded = new DateTime(2017, 2, 1),
                        StarRating = 4
                    },
                    new UserMovie
                    {
                        UserID = ids[1],
                        MovieID = "tt0988045",
                        FormatID = 5,
                        TitleSort = "Sherlock Holmes",
                        DateAdded = new DateTime(2017, 2, 2),
                        StarRating = 4,
                        Location = "Shelf 3"
                    },
                    new UserMovie
                    {
                        UserID = ids[2],
                        MovieID = "tt0169547",
                        FormatID = 4,
                        TitleSort = "American Beauty",
                        DateAdded = new DateTime(2017, 1, 31),
                        StarRating = 3,
                        Location = "Desk"
                    },
                    new UserMovie
                    {
                        UserID = ids[2],
                        MovieID = "tt0120815",
                        FormatID = 5,
                        TitleSort = "Saving Private Ryan",
                        DateAdded = new DateTime(2017, 2, 10),
                        StarRating = 5,
                        Location = "Bookcase",
                        Custom = "Kick Ass!!!"
                    },
                    new UserMovie
                    {
                        UserID = ids[3],
                        MovieID = "tt0458339",
                        FormatID = 4,
                        TitleSort = "Captain America: The First Avenger",
                        DateAdded = new DateTime(2016, 10, 14),
                        StarRating = 5,
                        Location = "Bookshelf",
                        Custom = "Awesome!!!"
                    }
                };

            context.UserMovies.AddRange(userMovies);
            context.SaveChanges();
        }
    }
}