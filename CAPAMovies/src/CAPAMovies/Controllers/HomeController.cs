﻿using System;
using CAPAMovies.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CAPAMovies.Controllers
{
    public class HomeController : Controller
    {
        [RequireHttps]
        public IActionResult Index()
        {
            /****************REDIRECT USER TO CONSTRUCTION PAGE****************/
            // Current datetime
            DateTime now = DateTime.Now.ToUniversalTime().AddHours(-8);

            // Project duedate (Tuesday, June 13 @ 6:00 PM on Finals Week)
            DateTime duedate = new DateTime(2017, 6, 6, 18, 0, 0);

            if (duedate.AddDays(-1) > now)
            {
                // Date difference
                TimeSpan diff = duedate - now;
                string remaining;

                if (diff.Days <= 30)
                {
                    remaining = diff.Days + " days, " + diff.Hours + " hours and " + diff.Minutes + " minutes remaining!";
                }
                else
                {
                    remaining = diff.Days + " days and " + diff.Hours + " hours remaining!";
                }

                // View variables
                ViewBag.Countdown = remaining;

                return View("Construction");
            }
            else
            {
                if (User.Identity.IsAuthenticated)
                {
                    return Redirect("../Movies/Index");
                }
                return View("../Account/Login");
            }
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        public ViewResult Construction()
        {
            ViewData["Message"] = "This site is under construction.";

            return View();
        }
    }
}
