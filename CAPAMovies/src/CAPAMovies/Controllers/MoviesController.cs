using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CAPAMovies.Data;
using CAPAMovies.Models;
using CAPAMovies.Models.MovieViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;

namespace CAPAMovies.Controllers
{
    [RequireHttps]
    [Authorize]
    public class MoviesController : Controller
    {
        private readonly IRepository _context; // Provides access to the custom added tables
        private readonly UserManager<ApplicationUser> _manager; // Provides access to the user information

        public ViewResult UsersList() => View(_context.Users.OrderBy(u => u.Email));
        public ViewResult FormatsList() => View(_context.Formats);
        public ViewResult MoviesList() => View(_context.Movies);
        public ViewResult UserMoviesList() => View(_context.UserMovies);

        public MoviesController(IRepository context, UserManager<ApplicationUser> manager)
        {
            _context = context;
            _manager = manager;
        }

        // GET: Movies
        public ViewResult Index(string SearchString, string sortOrder)
        {
            ViewData["TitleSort"] = String.IsNullOrEmpty(sortOrder) ? "title_desc" : "";
            ViewData["FormatSort"] = sortOrder == "Format" ? "format_desc" : "Format";
            ViewData["DateSort"] = sortOrder == "DateAdded" ? "date_desc" : "DateAdded";
            ViewData["StarSort"] = sortOrder == "StarRating" ? "star_desc" : "StarRating";
            ViewData["LocationSort"] = sortOrder == "Location" ? "location_desc" : "Location";
            ViewData["CustomSort"] = sortOrder == "Custom" ? "custom_desc" : "Custom";
            // Get a list of movies the user has and pass it to the view
            var userId = _manager.GetUserId(User);


            IEnumerable<UserMovie> UserMovieList = _context.UserMovies.Where(s => s.UserID == userId);
            List<Movie> mymovies = new List<Movie>();

 
            foreach(UserMovie mov in UserMovieList)
            {
                mymovies.Add((_context.Movies.SingleOrDefault(s => s.ID == mov.MovieID)));
            }
            IEnumerable<Movie> movies = mymovies;
            IEnumerable<Format> formats = _context.Formats;
            ViewBag.Movies = movies;
            ViewBag.Formats = formats;
            ViewBag.SearchString = SearchString;
            

            switch (sortOrder)
            {
                case "title_desc":
                    UserMovieList = UserMovieList.OrderByDescending(o => o.TitleSort);
                    break;
                case "format_desc":
                    UserMovieList = UserMovieList.OrderByDescending(o => o.FormatID);
                    break;
                case "date_desc":
                    UserMovieList = UserMovieList.OrderByDescending(o => o.DateAdded);
                    break;
                case "star_desc":
                    UserMovieList = UserMovieList.OrderByDescending(o => o.StarRating);
                    break;
                case "location_desc":
                    UserMovieList = UserMovieList.OrderByDescending(o => o.Location);
                    break;
                case "custom_desc":
                    UserMovieList = UserMovieList.OrderByDescending(o => o.Custom);
                    break;
                case "Format":
                    UserMovieList = UserMovieList.OrderBy(o => o.FormatID);
                    break;
                case "DateAdded":
                    UserMovieList = UserMovieList.OrderBy(o => o.DateAdded);
                    break;
                case "StarRatine":
                    UserMovieList = UserMovieList.OrderBy(o => o.StarRating);
                    break;
                case "Location":
                    UserMovieList = UserMovieList.OrderBy(o => o.Location);
                    break;
                case "Custom":
                    UserMovieList = UserMovieList.OrderBy(o => o.Custom);
                    break;
                default:
                    UserMovieList = UserMovieList.OrderBy(o => o.TitleSort);
                    break;
            }
            if (!string.IsNullOrEmpty(SearchString))
            {
                UserMovieList = UserMovieList.Where(s => s.TitleSort.ToUpper().Contains(SearchString.ToUpper()));
            }
            return View(UserMovieList);
        }

        // GET: Movies/Details
        public async Task<IActionResult> Details(string id)
        {
            id = id ?? Request.Query["movieID"];
            //TODO: Update this and view to show usermovie customizations and movie specifications
            if (String.IsNullOrEmpty(id))
            {
                // id was not passed
                return NotFound();
            }
            var currentuser = _manager.GetUserId(User);
            var userMovie =  _context.UserMovies.SingleOrDefault(m => m.MovieID == id && m.UserID == currentuser);
            var movie = _context.Movies.SingleOrDefault(m => m.ID == id);
            var myMovieDetails = await GetMovieDetailsAsync(movie.ID, false); //Get full movie plot length

            if (movie == null || myMovieDetails == null)
            {
                return NotFound();
            }
            var movieDetailsObj = JsonConvert.DeserializeObject<MovieDetails>(myMovieDetails);
            movieDetailsObj.myMovie = movie;
            movieDetailsObj.myUserMovie = userMovie;

            return View(movieDetailsObj);
        }

        /* Search for a movie to add to user's inventory */
        [HttpGet]
        public ViewResult Search()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Search([Bind("ID, Title, Year, PicLink")] Movie movie)
        {
            if (ModelState.IsValid)
            {
                if (!_context.Movies.Any(m => m.ID == movie.ID))
                {
                    _context.Add(movie);
                    await _context.SaveChangesAsync();
                }

                Format format = _context.Formats.SingleOrDefault(m => m.FormatName == "None");
                IEnumerable<Format> formats = _context.Formats;

                ViewBag.Movie = movie;
                ViewBag.UserMovie = new UserMovie(_manager.GetUserId(User), movie.ID, format.ID) {DateAdded = DateTime.Today.ToUniversalTime().AddHours(-8)};
                ViewBag.Formats = formats;
                return View("Create");
            }
            else
            {
                // there is a validation error
                return View();
            }
        }

        [HttpGet]
        public ViewResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("UserID, MovieID, FormatID, TitleSort, DateAdded, StarRating, Location, Custom, LoanedTo")] UserMovie usermovie)
        {
            //Make sure the user selected a format
            var format = _context.Formats.SingleOrDefault(m => m.FormatName == "None");
            if (usermovie.FormatID == format.ID)
            {
                ViewBag.Movie = _context.Movies.SingleOrDefault(m => m.ID == usermovie.MovieID);
                ViewBag.UserMovie = usermovie;
                ViewBag.Formats = _context.Formats;
                return View(usermovie);
            }

            //Make sure the user doesn't have that movie in the selected format
            if (_context.UserMovies.Any(m => m.MovieID == usermovie.MovieID
                                              && m.UserID == _manager.GetUserId(User)
                                              && m.FormatID == usermovie.FormatID))
            {
                ViewBag.Movie = _context.Movies.SingleOrDefault(m => m.ID == usermovie.MovieID);
                ViewBag.UserMovie = usermovie;
                ViewBag.Formats = _context.Formats;
                return View();
            }
            else
            {
                _context.Add(usermovie);
                await _context.SaveChangesAsync();
            }

            return RedirectToAction("Index");
        }
        

        // GET: Movies/Edit/5
        public IActionResult Edit(string id)
        {
            id = id ?? Request.Query["movieID"];

            string formatQueryID;
            formatQueryID = Request.Query["formatID"];

            int formatID = Convert.ToInt32(formatQueryID);
            if (String.IsNullOrEmpty(id))
            {
                return NotFound();
            }
            var movie = _context.Movies.SingleOrDefault(m => m.ID == id);
            if (movie == null)
            {
                return NotFound();
            }

            Format format = _context.Formats.SingleOrDefault(m => m.FormatName == "None");
            IEnumerable<Format> formats = _context.Formats;

            var userId = _manager.GetUserId(User);
            ViewBag.Movie = movie;
            ViewBag.UserMovie = _context.UserMovies.SingleOrDefault(s => (s.UserID == userId) &&
                                                                         (s.MovieID == movie.ID) &&
                                                                         (s.FormatID == formatID));
            ViewBag.Formats = formats;
            return View("Edit");
        }

        // POST: Movies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit([Bind("UserID, MovieID, FormatID, TitleSort, DateAdded, StarRating, Location, Custom, LoanedTo")] UserMovie usermovie)
        {
            var updatedUserMovie =  _context.UserMovies.SingleOrDefault(m => m.MovieID == usermovie.MovieID
                                                                            && m.UserID == _manager.GetUserId(User)
                                                                            && m.FormatID == usermovie.FormatID);

            updatedUserMovie.StarRating = usermovie.StarRating;
            updatedUserMovie.Custom = usermovie.Custom;
            updatedUserMovie.TitleSort = usermovie.TitleSort;
            updatedUserMovie.Location = usermovie.Location;
            updatedUserMovie.LoanedTo = usermovie.LoanedTo;
            
            _context.Update(updatedUserMovie);
            await _context.SaveChangesAsync();

             return RedirectToAction("Index");
        }

        // GET: Movies/Delete/5
        public IActionResult Delete(string UserID, string MovieID, int FormatID, bool? saveChangesError = false)
        {
            if (UserID == null || MovieID == null || FormatID == 0)
            {
                return NotFound();
            }

            var usermovie = _context.UserMovies.SingleOrDefault(m => m.UserID == UserID && m.MovieID == MovieID && m.FormatID == FormatID);
            if (usermovie == null)
            {
                return NotFound();
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ViewData["ErrorMessage"] =
                    "Delete failed. Try again, and if the problem persists " +
                    "contact the project owner.";
            }
            ViewBag.Formats = _context.Formats;
            return View(usermovie);
        }

        // POST: Movies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string UserID, string MovieID, int FormatID)
        {
            var usermovie = _context.UserMovies.SingleOrDefault(m => m.UserID == UserID && m.MovieID == MovieID && m.FormatID == FormatID);
            if (usermovie == null)
            {
                return RedirectToAction("Index");
            }

            try
            {
                _context.Remove(usermovie);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            catch (DbUpdateException)
            {
                ViewBag.Formats = _context.Formats;
                return RedirectToAction("Delete",
                    new {UserID = UserID, MovieID = MovieID, FormatID = FormatID, saveChangesError = true});
            }
        }

        public IActionResult DeleteUser(string UserID, bool? saveChangesError = false)
        {
            if (UserID == null)
            {
                return NotFound();
            }

            var user = _context.Users.SingleOrDefault(u => u.Id == UserID);
            if (user == null)
            {
                return NotFound();
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ViewData["ErrorMessage"] =
                    "Delete failed. Try again, and if the problem persists " +
                    "contact the project owner.";
            }
            return View(user);
        }

        [HttpPost, ActionName("DeleteUser")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteUserConfirmed(string UserID)
        {
            var user = _context.Users.SingleOrDefault(u => u.Id == UserID);
            if (user == null)
            {
                return RedirectToAction("UsersList");
            }

            try
            {
                _context.Remove(user);
                await _context.SaveChangesAsync();
                return RedirectToAction("UsersList");
            }
            catch (DbUpdateException)
            {
                return RedirectToAction("DeleteUser",
                    new {UserID = UserID, saveChangesError = true});
                throw;
            }
        }

        private bool MovieExists(string id)
        {
            return _context.Movies.Any(e => e.ID == id);
        }

        private static async Task<String> GetMovieDetailsAsync(string id, bool bShortPlot=true)
        {
            string plotLength = "&plot=short&r=json";
            if (!bShortPlot)
                plotLength = "&plot=full&r=json";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://www.omdbapi.com");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("?i=" + id + plotLength+"&apikey=ed229b04");

                if (response.IsSuccessStatusCode)
                {
                    Console.WriteLine("Success");
                }
                else
                {
                    Console.WriteLine("Error with feed");
                }
                String omdbDetails = await response.Content.ReadAsStringAsync();
                return omdbDetails;
            }
        }
    }
}
