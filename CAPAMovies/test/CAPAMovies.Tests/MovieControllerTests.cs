﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using CAPAMovies.Controllers;
using CAPAMovies.Data;
using CAPAMovies.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Xunit;
using Moq;

namespace CAPAMovies.Tests
{
    public class MovieControllerTests
    {
        private class FakeUserManager : UserManager<ApplicationUser>
        {
            public FakeUserManager()
                : base(new Mock<IUserStore<ApplicationUser>>().Object,
                  new Mock<IOptions<IdentityOptions>>().Object,
                  new Mock<IPasswordHasher<ApplicationUser>>().Object,
                  new IUserValidator<ApplicationUser>[0],
                  new IPasswordValidator<ApplicationUser>[0],
                  new Mock<ILookupNormalizer>().Object,
                  new Mock<IdentityErrorDescriber>().Object,
                  new Mock<IServiceProvider>().Object,
                  new Mock<ILogger<UserManager<ApplicationUser>>>().Object)
            { }

            public override Task<IdentityResult> CreateAsync(ApplicationUser user, string password)
            {
                return Task.FromResult(IdentityResult.Success);
            }
        }

        private class FakeSignInManager : SignInManager<ApplicationUser>
        {
            public FakeSignInManager(IHttpContextAccessor contextAccessor)
                : base(new FakeUserManager(),
                  contextAccessor,
                  new Mock<IUserClaimsPrincipalFactory<ApplicationUser>>().Object,
                  new Mock<IOptions<IdentityOptions>>().Object,
                  new Mock<ILogger<SignInManager<ApplicationUser>>>().Object)
            { }

            public override Task SignInAsync(ApplicationUser user, bool isPersistent, string authenticationMethod = null)
            {
                return Task.FromResult(0);
            }

            public override Task<SignInResult> PasswordSignInAsync(string userName, string password, bool isPersistent, bool lockoutOnFailure)
            {
                return Task.FromResult(SignInResult.Success);
            }

            public override Task SignOutAsync()
            {
                return Task.FromResult(0);
            }
        }

        public ApplicationUser[] GetValidUsers()
        {
            string[] email = { "admin@user.com", "crystalstevens@capamovies.com", "alexhouston@capamovies.com", "paulivanov@capamovies.com", "aaronday@capamovies.com" };
            ApplicationUser[] users = new ApplicationUser[email.Length];
            for (var i = 0; i < email.Length; ++i)
            {
                var user = new ApplicationUser() {UserName = email[i], Email = email[i], EmailConfirmed = true};
                users[i] = user;
            }
            return users;
        }

        public Format[] GetValidFormats()
        {
            return new[]
            {
                new Format { FormatName =  "None", Physical = false },
                new Format { FormatName =  "DVD", Physical = true },
                new Format { FormatName =  "Blu-Ray", Physical = true },
                new Format { FormatName =  "3D Blu-Ray", Physical = true },
                new Format { FormatName =  "iTunes", Physical = false },
                new Format { FormatName =  "UltraViolet", Physical = false },
                new Format { FormatName =  "MP4", Physical = false },
                new Format { FormatName =  "AVI", Physical = false },
                new Format { FormatName =  "HD", Physical = true },
                new Format { FormatName =  "VHS", Physical = true }
            };
        }

        public Movie[] GetValidMovies()
        {
            return new[]
            {
                new Movie
                {
                    ID = "ID1",
                    PicLink = "http://www.starwars.com",
                    Title = "Star Wars",
                    Year = 1973
                },
                new Movie
                {
                    ID = "ID2",
                    PicLink = "http://www.marval.com/captain_america.html",
                    Title = "Captain America",
                    Year = 2013
                },
                new Movie
                {
                    ID = "ID3",
                    PicLink = "http://www.spr.com",
                    Title = "Saving Private Ryan",
                    Year = 1998
                },
                new Movie
                {
                    ID = "ID4",
                    PicLink = "http://www.gladiator.com",
                    Title = "Gladiator",
                    Year = 2000
                },
                new Movie
                {
                    ID = "ID5",
                    PicLink = "http://www.sherlock.com",
                    Title = "Sherlock Holmes",
                    Year = 2009
                },
                new Movie
                {
                    ID = "ID6",
                    PicLink = "http://www.bourne.com",
                    Title = "The Bourne Identity",
                    Year = 2002
                },
                new Movie
                {
                    ID = "ID7",
                    PicLink = "http://www.avatar.com",
                    Title = "Avatar",
                    Year = 2009
                },
                new Movie
                {
                    ID = "ID8",
                    PicLink = "http://www.forestgump.com",
                    Title = "Forest Gump",
                    Year = 1994
                },
                new Movie
                {
                    ID = "ID9",
                    PicLink = "http://www.goodfellas.com",
                    Title = "Goodfellas",
                    Year = 1994
                },
                new Movie
                {
                    ID = "ID0",
                    PicLink = "http://www.americanbeauty.com",
                    Title = "American Beauty",
                    Year = 1999
                }
            };
        }

        public UserMovie[] GetValidUserMovies()
        {
            var ids = GetValidUsers().Select(u => u.Id).ToList();
            return new[]
            {
                new UserMovie
                {
                    UserID = ids[0],
                    MovieID = "ID1",
                    FormatID = 2,
                    TitleSort = "Star Wars",
                    DateAdded = new DateTime(2017, 1, 1),
                    StarRating = 3,
                    Location = "Shelf 1"
                },
                new UserMovie
                {
                    UserID = ids[0],
                    MovieID = "ID1",
                    FormatID = 3,
                    TitleSort = "Star Wars",
                    DateAdded = new DateTime(2017, 1, 2),
                    StarRating = 3,
                    Location = "Shelf 1"
                },
                new UserMovie
                {
                    UserID = ids[0],
                    MovieID = "ID6",
                    FormatID = 6,
                    TitleSort = "Bourne Identity, The",
                    DateAdded = new DateTime(2017, 2, 1),
                    StarRating = 4
                },
                new UserMovie
                {
                    UserID = ids[1],
                    MovieID = "ID5",
                    FormatID = 5,
                    TitleSort = "Sherlock Holmes",
                    DateAdded = new DateTime(2017, 2, 2),
                    StarRating = 4,
                    Location = "Shelf 3"
                },
                new UserMovie
                {
                    UserID = ids[2],
                    MovieID = "ID0",
                    FormatID = 4,
                    TitleSort = "American Beauty",
                    DateAdded = new DateTime(2017, 1, 31),
                    StarRating = 3,
                    Location = "Desk"
                },
                new UserMovie
                {
                    UserID = ids[2],
                    MovieID = "ID3",
                    FormatID = 5,
                    TitleSort = "Saving Private Ryan",
                    DateAdded = new DateTime(2017, 2, 10),
                    StarRating = 5,
                    Location = "Bookcase",
                    Custom = "Kick Ass!!!"
                },
                new UserMovie
                {
                    UserID = ids[3],
                    MovieID = "ID2",
                    FormatID = 4,
                    TitleSort = "Captain America",
                    DateAdded = new DateTime(2016, 10, 14),
                    StarRating = 5,
                    Location = "Bookshelf",
                    Custom = "Awesome!!!"
                }
            };
        }

        [Fact]
        public void ListUsers()
        {
            //Arrange
            Mock<IRepository> mock = new Mock<IRepository>();
            mock.Setup(m => m.Users).Returns(GetValidUsers());

            MoviesController controller = new MoviesController(mock.Object, new FakeUserManager());

            //Act
            IEnumerable<ApplicationUser> result = controller.UsersList().ViewData.Model as IEnumerable<ApplicationUser>;

            //Assert
            ApplicationUser[] users = result.ToArray();
            Assert.True(users.Length == 5);
            Assert.Equal("admin@user.com", users[1].Email);
            Assert.Equal("aaronday@capamovies.com", users[0].Email);
        }

        [Fact]
        public void ListFormats()
        {
            //Arrange
            Mock<IRepository> mock = new Mock<IRepository>();
            mock.Setup(m => m.Formats).Returns(GetValidFormats());

            MoviesController controller = new MoviesController(mock.Object, new FakeUserManager());

            //Act
            IEnumerable<Format> result = controller.FormatsList().ViewData.Model as IEnumerable<Format>;

            //Assert
            Format[] formats = result.ToArray();
            Assert.True(formats.Length == 10);
            Assert.Equal("None", formats[0].FormatName);
            Assert.Equal("VHS", formats[9].FormatName);
        }

        [Fact]
        public void ListMovies()
        {
            //Arrange
            Mock<IRepository> mock = new Mock<IRepository>();
            mock.Setup(m => m.Movies).Returns(GetValidMovies());

            MoviesController controller = new MoviesController(mock.Object, new FakeUserManager());

            //Act
            IEnumerable<Movie> result = controller.MoviesList().ViewData.Model as IEnumerable<Movie>;

            //Assert
            Movie[] movies = result.ToArray();
            Assert.True(movies.Length == 10);
            Assert.Equal("Star Wars", movies[0].Title);
            Assert.Equal("American Beauty", movies[9].Title);
        }

        [Fact]
        public void ListUserMovies()
        {
            //Arrange
            Mock<IRepository> mock = new Mock<IRepository>();
            mock.Setup(m => m.UserMovies).Returns(GetValidUserMovies());

            MoviesController controller = new MoviesController(mock.Object, new FakeUserManager());

            //Act
            IEnumerable<UserMovie> result = controller.UserMoviesList().ViewData.Model as IEnumerable<UserMovie>;

            //Assert
            UserMovie[] usermovies = result.ToArray();
            Assert.True(usermovies.Length == 7);
            Assert.Equal("Star Wars", usermovies[0].TitleSort);
            Assert.Equal("Captain America", usermovies[6].TitleSort);
        }
        /*
        [Fact]
        public void DefaultMovieDate()
        {
            CAPAMovies.Models.Movie testMovie = new Movie();
            string DateString = "01/01/0001 00:00:00";
            Assert.Equal(DateString, testMovie.DateAdded.ToString());
        }

        [Fact]
        public void DefaultMovieFormat()
        {
            CAPAMovies.Models.Movie testMovie = new Movie();
            Assert.Equal(null, testMovie.Format);
        }

        [Fact]
        public void DefaultMovieimdbdid()
        {
            CAPAMovies.Models.Movie testMovie = new Movie();
            Assert.Equal(null, testMovie.IMDBID);
        }

        [Fact]
        public void DefaultMovieLocation()
        {
            CAPAMovies.Models.Movie testMovie = new Movie();
            Assert.Equal(null, testMovie.Location);
        }

        [Fact]
        public void DefaultMovieMovieid()
        {
            CAPAMovies.Models.Movie testMovie = new Movie();
            Assert.Equal(0, testMovie.MovieID);
        }

        [Fact]
        public void DefaultMovieQuantity()
        {
            CAPAMovies.Models.Movie testMovie = new Movie();
            Assert.Equal(0, testMovie.Quantity);
        }

        [Fact]
        public void DefaultMovieStarRating()
        {
            CAPAMovies.Models.Movie testMovie = new Movie();
            Assert.Equal(null, testMovie.StarRating);
        }

        [Fact]
        public void DefaultMovieTitleSort()
        {
            CAPAMovies.Models.Movie testMovie = new Movie();
            Assert.Equal(null, testMovie.TitleSort);
        }

        [Fact]
        public void DefaultMovieUserMovies()
        {
            CAPAMovies.Models.Movie testMovie = new Movie();
            Assert.Equal(null, testMovie.UserMovies);
        }

        [Fact]
        public void CorrectUserID()
        {
            CAPAMovies.Models.UserMovie testUser = new UserMovie("ID", 0);
            Assert.Equal("ID", testUser.UserID);
        }

        [Fact]
        public void CorrectMovieID()
        {
            CAPAMovies.Models.UserMovie testUser = new UserMovie("ID", 0);
            Assert.Equal(0, testUser.MovieID);
        }
        */
    }
}
